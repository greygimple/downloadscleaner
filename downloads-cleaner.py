###### ------ IMPORTS ------ ######
import os
import shutil
import time
import datetime
import tarfile


###### ------ CONSTANTS ------ ######

# file type to folder map
fileExtFolder = {
    'audio':'Media/Audio/',
    'video':'Media/Video/',
    'image':'Media/Image/',
    'comp_archive':'File/CompArc/',
    'font':'File/Font/',
    'executables':'File/Executables/',
    'system':'File/System/',
    'document':'File/Document/',
    'webpage':'Program/Webpage/',
    'db':'Program/Database/',
    'code':'Program/Code/'
}

# file extention to file type map
fileTypeFileExt = {
    # Media
    'audio':('.8svx','.16svx','.aiff','.au','.bwf','.cdda','.dsf','.dff','.wav','.ra','.rm','.flac','.la','.pac','.ape','.ofr','.ofs','.off','.rka','.rkau','.shn','.tak','.thd','.tta','.wv','.wma','.brstm','.dts','.dtshd','.dtsma','.ast','.aw','.psf','.ac3','.amr','.mp1','.mp2','.mp3','.mpeg','.spx','.gsm','.aac','.mpc','.vqf','.ots','.swa','.vox','.voc','.dwd','.smp','.ogg','.mod','.mt2','.s3m','.xm','.it','.nsf','.mid','.ftm','.abc','.darms','.etf','.gp','.kern','.ly','.mei','.mus','.musx','.mxl','.mscx','.mxcz','.smdl','.sib','.niff','.ptb','.cust','.gym','.jam','.rmj','.sid','.spc','.txm',',vgm','.ym','.pvd'),
    'video':('.webm','.mkv','.flv','.vob','.ogv','.drc','.mng','.mts','.m2ts','.ts','.mov','.qt','.wmv','.yuv','.rmvb','.asf','.amv','.mp4','.m4p','.mpg','.mpv','.svi','.3pg','.mxf','.roq','.nsv','.f4v','.f4a','.f4b'),
    'image':('.webp', '.jfif','.act','.ase','.gpl','.pal','.icc','.icm','.art','.blp','.bmp','.bti','.cd5','.cit','.cr2','.clip','.dds','.dib','.djvu','.exif','.gif','.gifv','.grf','.iff','.jng','.jpg','.jpeg', '.jp2','.jps','.kra','.lbm','.max','.miff','.msp','.nitf','.otb','.pbm','.pc1','.pc2','.pc3','.pcx','.pdn','.pgm','.pi1','.pi2','.pi3','.pict','.pct','.png','.pnm','.pns','.ppm','.psb','.pdd','.psp','.px','.pxm','.pxr','.qfx','.raw','.rle','.sct','.sgi','.tga','.tiff','.vtf','.xbm','.xcf','.xpm','.zif','.3dv','.amf','.awg','.ai','.cgm','.cdr','.cmx','.dp','.dxf','.e2d','.fs','.gbr','.odg','.svg','.stl','.vrml','.x3d','.sxd','.tgax','.v2d','.vdoc','.vsd','.vsdx','.vnd','.wmf','.emf','.xar'),
    #Program
    'webpage':('.dtd','.html','.xhtml','.mhtml','.maf','.maff','.asp','.aspx','.adp','.bml','.cfm','.cgi','.ihtml','.jsp','.las','.lasso','.lassoapp','.pl','.php','.ssi'),
    'code':('.adb','.ads','.ahk','.applescript','.as','.au3','.bat','.bas','.btm','.class','.cljs','.cmd','.coffee','.c','.cpp','.cs','.ino','.egg','.erb','.go','.hta','.ibi','.ici','.ijs','.ipynyb','.itcl','.js','.jsfl','.kt','.lua','.m','.mrc','.ncf','.nuc','.nud','.nut','.o','.pde','.pm','.ps1','.ps1xml','.psc1','.psd1','.psm1','.py','.pyc','.pyo','.pyw','.r','.rb','.rdp','.red','.rs','.sb2','.scpt','.scptd','.sdl','.sh','.syjs','.sypy','.tcl','.tns','.vbs','.xpl','.ebuild'),
    'database':('.4db','.4dd','.4dindy','.4dindx','.4dr','.accdb','.accde','.adt','.apr','.box','.chml','.daf','.dat','.db','.dbf','.dta','.ess','.eap','.fdb','.fp,','.frm','.gdb','.gtable','.kexi','.kexic','.kexis','.ldb','.lirs','.mda','.mdb','.adp','.mde','.mdf','.myd','.myi','.ncf','.nsf','.ntf','.nv2','.odb','.ora','.pcontact','.pdb','.pdi','.pdx','.prc','.sql','.rec','.rel','.rin','.sdb','.sdf','.sqlite','.udl','.wadata','.waindx','.wamodel','.wajournal','.wdb','.wmdb'),
    # File
    'comp_archive':('.tbz2', '.bz2', '.gz', '.jar', '.?mn','.main','..?q?','.7z','.aapkg','.ace','.alz','.appx','.at3','.bke','.arc','.arj','.ass','.b','.ba','.big','.bjsn','.bkf','.bzip2','.bld','.cab','.c4','.cals','.xaml','.clipflair','.cpt,','.daa','.deb','.dmg','.ddz','.dn','.dpe','.ecab','.esd','.ess','.flipchart','.gbp','.gbs','.gho','.gzip','.ipg','.lbr','.lqr','.lha','.lzip','.lzo','.lzma','.lzx','.mbw','.mpq','.bin','.nl2pkg','.nth','.oar','.osk','.osr','.osz','.pak','.par','.paf','.pea','.rar','.rag,','.rax','.rbxl','.rbxlx','.rpm','.sb','.sb3','.sen','.sit','.sis','.sisx','.skb','.sq','.swm','.szs','.tar','.tgz','.tb','.tib','.uha','.uue','.vol','.vsa','.wax','.wim','.xap','.xz','.z','.zoo','.zip'),
    'executables':('.msi','.apk','.com','.exe','.gadget','.wsf'),
    'font':('.abf','.afm','.bdf','.bmf','.brfnt','.fnt','.fon','.mgf','.otf','.postscript','.pfa','.pfb','.pfm','.fond','.sfd','.snf','.tdf','.tfm','.ttf','.ufo','.woff'),
    'system':('.bak','.cfg','.cpl','.cur','.dll','.dmp','.drv','.icns','.ico','.ini','.sys', '.conf'),
    'document':('.0','.1st','.600','.602','.abw','.acl','.afp','.ami','.amigaguide','.ans','.asc','.aww','.ccf','.csv','.cwk','.dbk','.dita','.doc','.docm','.docx','.dot','.dotm','.dotx','.epub','.ezw','.fdx','.ftm','.fx','.gdoc','.hwp','.hwpml','.log','.lwp','.mbp','.md','.me','.mcw','.mobi','.nb','.nbp','.neis','.nt','.nq','.odm','.odoc','.odt','.ods','.osheet','.ott','.omm','.pages','.pap','.pdax','.pdf','.quox','.radix-64','.rtf','.rpt','.sdw','.se','.stw','.sxw','.tex','.info','.troff','.txt','.uof','.uoml','.via','.wpd','.wps','.wpt','.wrd','.wrf','.xps','.pot','.potm','.potx','.ppa','.ppam','.pps','.ppsm','.ppsx','.ppt','.pptm','.pptx')
}

# starting directory
startDirectory = "/home/grey/Downloads/"
# ending directory
endDirectory = "/home/grey/Downloads/"

# list of files
files = []
# list of directories
directories = []

# list of files removed form sorting
xFiles = []
# list of directories removed form sorting
xDirectories = []

# list of start paths
startPaths = []
# list of final paths without file
endPathsDirs = []
# list of final paths
endPaths = []


###### ------ FUNCTIONS ------ ######

### --- SCAN FOR FILES AND DIRECTORIES --- ###

def scanFiles():
    # grabs everything in directory and checks if it is a file
    files = [file for file in os.listdir(startDirectory) 
                if os.path.isfile(os.path.join(startDirectory, file))]
    return files

def scanDirectories():
    # list of directories
    # grabs everything in directory and checks if it is a directory
    directories = [dir for dir in os.listdir(startDirectory) 
                if os.path.isdir(os.path.join(startDirectory, dir)) and dir != "Media" and dir != "File" and dir != "Program"]
    return directories

### --- GENERATE XFILES LIST --- ###

def generateXFiles(files):
    xFiles = []
    for file in files:
        xFiles.append(' ')
    return xFiles

### --- GENERATE XDIRECTORIES LIST --- ###

def generateXDirectories(directories):
    xDirectories = []
    for directory in directories:
        xDirectories.append(' ')
    return xDirectories

### --- CREATE FILE SORT SCHEME --- ###

def createFileScheme(xFiles, files):
    for file in files:
        if xFiles[files.index(file)] == ' ':
            # start paths
            startPath = startDirectory + file
            startPaths.append(startPath)

            # end paths
            # split file name into name and extension
            fileName, fileExt = os.path.splitext(file)

            # get the end path mapped to the file extension
            breakFlag = False
            for key in fileTypeFileExt:
                for value in fileTypeFileExt[key]:
                    if value == fileExt:
                        fileTypeFileExtMap = key
                        ignore = False
                        breakFlag = True
                        break
                    else:
                        ignore = True
                if breakFlag:
                    break
            
            if ignore == True:
                endPath = endDirectory
                endPathsDirs.append(endPath)
                endPath += file
            else:
                endPath = endDirectory + fileExtFolder[fileTypeFileExtMap]

                # add the date to the file path
                # date last modified
                fileDate = os.path.getmtime(startPath)
                # convert fileDate from a time stamp to datetime format
                fileDateConverted = datetime.datetime.fromtimestamp(fileDate)
                # get the year
                year = fileDateConverted.strftime("%Y")
                # get the month
                month = fileDateConverted.strftime("%m")
                # add date to end path
                endPath += year + "/" + month + "/"
                
                endPathsDirs.append(endPath)

                # file clone detection
                # test file name
                testEndPath = endPath + file
                # increamental value
                i = 0
                # check if it exists already
                while os.path.exists(testEndPath):
                    # increment variable
                    i += 1
                    # make new testend path
                    testEndPath = endPath + fileName + " (" + str(i) + ")" + fileExt
                # set the file end path
                endPath = testEndPath
            # add the path to list of end paths
            endPaths.append(endPath)
    return startPaths, endPaths, endPathsDirs

### --- OUTPUT FILE SORT SCHEME --- ###

def outputFileSortScheme(files, xFiles):
    for file in files:
        x = len(endDirectory)
        fileIndex = files.index(file)
        endPathRipped = endPaths[fileIndex][x:]
        
        print(str(fileIndex) + " [" + xFiles[fileIndex] + "] " + file + " >>> " + endPathRipped)

### --- OUTPUT DIRECTORY SORT SCHEME --- ###

def outputDirectorySortScheme(directories, xDirectories):
    for directory in directories:
        directoryIndex = directories.index(directory)
        print(str(directoryIndex) + " [" + xDirectories[directoryIndex] + "] " + directory)

### --- EXPORT FILE SORT SCHEME --- ###

def export(startPaths, endPaths, endPathsDirs, xFiles):
    for endPath in endPaths:
        endPathIndex = endPaths.index(endPath)
        if xFiles[endPathIndex] == ' ':
            if not os.path.exists(endPathsDirs[endPathIndex]):
                os.makedirs(str(endPathsDirs[endPathIndex]))
            os.rename(startPaths[endPathIndex], endPath)

### --- EXPORT DIRECTORY SORT SCHEME --- ###

def compressExport(directories, xDirectories, endPaths, endPathsDirs, xFiles, files, startPaths):
    for directory in directories:
        directoryIndex = directories.index(directory)
        outputCompressedName = endDirectory + directory + ".tar.gz"
        sourceDirectory = startDirectory + directory
        with tarfile.open(outputCompressedName, "w:gz") as tar:
            tar.add(sourceDirectory, arcname=os.path.basename(sourceDirectory))
        shutil.rmtree(sourceDirectory)
    files.clear()
    files = scanFiles()
    xFiles.clear()
    xFiles = generateXFiles(files)
    startPaths.clear()
    endPaths.clear()
    endPathsDirs.clear()
    startPaths, endPaths, endPathsDirs = createFileScheme(xFiles, files)
    export(startPaths, endPaths, endPathsDirs, xFiles)

###### ------ MAIN LOOP FUNCTON ------ ######

def main(files, xFiles, directories, xDirectories, startPaths, endPathsDirs, endPaths):
    files = scanFiles()
    directories = scanDirectories()
    xFiles = generateXFiles(files)
    xDirectories = generateXDirectories(directories)
    startPaths, endPaths, endPathsDirs = createFileScheme(xFiles, files)

    if len(files) != 0 or len(directories) != 0:
        if len(files) > 0:
            fileMode = True
        else:
            fileMode = False
        
        exitProgram = False
        while not exitProgram:
            os.system("clear")
            if fileMode:
                print()
                print(" Files Mode")
                print()
                
                outputFileSortScheme(files, xFiles)
                
                print()
                print(" Select")
                print(" x - where x is the index of the file in sorting scheme")
                print("     this selected file will be toggled to be sorted or not")
                print("     files marked with 'x' will not be sorted")
                print(" e - export sorting scheme, sorting the files")
                print(" r - reset all file marks to default, to be sorted")
                print(" d - directory mode, wont enter if there are no directories")
                print(" q - quit program")
                print()

                option = input("Selection: ")

                xSelect = True
                try:
                    optionInt = int(option)
                    xSelect = True
                except:
                    xSelect = False
                if xSelect:
                    if optionInt >= 0 and optionInt < len(files):
                        if xFiles[optionInt] == ' ':
                            xFiles[optionInt] = 'x'
                        elif xFiles[optionInt] == 'x':
                            xFiles[optionInt] = ' '
                        else:
                            print()
                            print(" Something Has Gone Wrong")
                            print("Quitting")
                            print()
                            exit()
                    else:
                        print()
                        print("Invalid Selection")
                        print("File Indexed " + str(optionInt) + " Does Not Exist")
                else:
                    if option == "q":
                        print()
                        print("Quitting Program")
                        print()
                        exitProgram = True
                        os.system("clear")
                    elif option == "r":
                        xFiles = generateXFiles(files)
                    elif option == "e":
                        export(startPaths, endPaths, endPathsDirs, xFiles)
                    elif option == "d":
                        if len(directories) > 0:
                            fileMode = False
                    else:
                        print()
                        print("Invalid Option")
            else:
                print()
                print(" Directory Mode")
                print()

                outputDirectorySortScheme(directories, xDirectories)

                print()
                print(" Select")
                print(" x - where x is the index of the directory in sorting scheme")
                print("     this selected directory will be toggled to be sorted or not")
                print("     directories marked with 'x' will not be sorted")
                print(" e - export sorting scheme, compressing and sorting the directories")
                print(" r - reset all directory marks to default, to be compressed and sorted")
                print(" f - file mode, wont enter if there are no files")
                print(" q - quit program")
                print()

                option = input("Selection: ")

                xSelect = True
                try:
                    optionInt = int(option)
                    xSelect = True
                except:
                    xSelect = False
                if xSelect:
                    if optionInt >= 0 and optionInt < len(directories):
                        if xDirectories[optionInt] == ' ':
                            xDirectories[optionInt] = 'x'
                        elif xDirectories[optionInt] == 'x':
                            xDirectories[optionInt] = ' '
                        else:
                            print()
                            print(" Something Has Gone Wrong")
                            print("Quitting")
                            print()
                            exit()
                    else:
                        print()
                        print("Invalid Selection")
                        print("File Indexed " + str(optionInt) + " Does Not Exist")
                else:
                    if option == "q":
                        print()
                        print("Quitting Program")
                        print()
                        exitProgram = True
                        os.system("clear")
                    elif option == "r":
                        xDirectories = generatexDirectories(files)
                    elif option == "e":
                        compressExport(directories, xDirectories, endPaths, endPathsDirs, xFiles, files, startPaths)
                    elif option == "f":
                        if len(files) > 0:
                            fileMode = True
                    else:
                        print()
                        print("Invalid Option")
    else:
        print()
        print("Downloads Directory Empty")
        print("Nothing To Do")
        print()


###### ------ RUN MAIN LOOP FUNCTION ------ ######
if __name__ == '__main__':
    main(files, xFiles, directories, xDirectories, startPaths, endPathsDirs, endPaths)
